import {gql} from 'apollo-boost';

const Karyawans = gql`
    {
      Karyawans {
        _id
        nama
        bagian
        gaji_pokok
        uang_makan
        transport
        apresiasi
      }
    }
`;

const AddKaryawan = gql`
    mutation ($nama: String!, $bagian: String!, $gaji_pokok: Int!, $uang_makan: Int, $transport: Int, $apresiasi: Int)
    {
      createKaryawan
      (input: {
        nama: $nama
        bagian: $bagian
        gaji_pokok: $gaji_pokok
        uang_makan: $uang_makan
        transport: $transport
        apresiasi: $apresiasi
      })
      {
        _id
        nama
        bagian
        gaji_pokok
        uang_makan
        transport
        apresiasi
      }
    }   
`;

const DelKaryawan = gql`
    mutation ($id: ID!)
    {
      deleteKaryawan(_id: $id){
        _id
      }
    }
`;

const UpdateKaryawan = gql`
    mutation ($id: ID!, $nama: String!, $bagian: String!, $gaji_pokok: Int!, 
            $uang_makan: Int, $transport: Int, $apresiasi: Int) 
    {
      updateKaryawan
      (
        _id: $id, 
        input: {
            nama: $nama
            bagian: $bagian
            gaji_pokok: $gaji_pokok
            uang_makan: $uang_makan
            transport: $transport
            apresiasi: $apresiasi
        }
      )
      {
        _id
        nama
        bagian
        gaji_pokok
        uang_makan
        transport
        apresiasi
      }
    }
`;

const Periodes = gql`
    {
      Periodes {
        _id
        startDate
        endDate
      }
    }
`;

const PeriodesByDate = gql`
    {
      PeriodesByDate {
        _id
        startDate
        endDate
      }
    }
`;

const AddPeriode = gql`
    mutation ($startDate: String!, $endDate: String!)
    {
      createPeriode(input: {
        startDate: $startDate
        endDate: $endDate
      })
      {
        _id
        startDate
        endDate
      }
    }   
`;

const UpdatePeriode = gql`
    mutation ($id: ID!, $startDate: String!, $endDate: String!)
    {
      updatePeriode(
        _id: $id, 
        input:{
            startDate: $startDate
            endDate: $endDate
        }
      )
      {
        _id
        startDate
        endDate
      }
    }   
`;

const DelPeriode = gql`
    mutation ($id: ID!)
    {
      deletePeriode(_id: $id){
        _id
      }
    }
`;

const Gajians = gql`
    {
        Gajians {
            _id
            id_periode
            id_karyawan
            jam_lembur
            jumlah_hari
            total_gaji
            tanggal_pembayaran
        }
    }
`;

const AddGajian = gql`
    mutation ($id_periode: String!, $id_karyawan: String!, $jam_lembur: Float!, $jumlah_hari: Float!, $total_gaji: Int!, $tanggal_pembayaran: String!)
    {
      createGajian(input: {
        id_periode: $id_periode
        id_karyawan: $id_karyawan
        jam_lembur: $jam_lembur
        jumlah_hari: $jumlah_hari
        total_gaji: $total_gaji
        tanggal_pembayaran: $tanggal_pembayaran
      })
      {
        _id
        id_periode
        id_karyawan
        jam_lembur
        jumlah_hari
        total_gaji
        tanggal_pembayaran
      }
    }   
`;

const UpdateGajian = gql`
    mutation ($id: ID!, $id_periode: String!, $id_karyawan: String!, $jam_lembur: Float!, $jumlah_hari: Float!, $total_gaji: Int!, $tanggal_pembayaran: String!)
    {
      updateGajian(
        _id: $id, 
        input:{
            id_periode: $id_periode
            id_karyawan: $id_karyawan
            jam_lembur: $jam_lembur
            jumlah_hari: $jumlah_hari
            total_gaji: $total_gaji
            tanggal_pembayaran: $tanggal_pembayaran
        }
      )
      {
        _id
        id_periode
        id_karyawan
        jam_lembur
        jumlah_hari
        total_gaji
        tanggal_pembayaran
      }
    }   
`;

const DelGajian = gql`
    mutation ($id: ID!)
    {
      deleteGajian(_id: $id){
        _id
      }
    }
`;


export {Karyawans, AddKaryawan, DelKaryawan, UpdateKaryawan,
        Periodes, AddPeriode, DelPeriode, UpdatePeriode, PeriodesByDate,
        Gajians, AddGajian, DelGajian, UpdateGajian};