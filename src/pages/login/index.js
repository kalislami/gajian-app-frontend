import React, {Component} from 'react';
import { Col, Row, FormGroup, Container, Alert } from 'reactstrap';
import AuthContext from '../../context/auth-context';
import Loader from '../../component/loader/loader';

import 'bootstrap/dist/css/bootstrap.min.css';
import { apiConfig } from '../../config/config';

class Login extends Component {

    static contextType = AuthContext;

    state = {
        submit: false,
        visible: false
    };

    constructor(props) {
        super(props);
        this.username = React.createRef();
        this.password = React.createRef();
    }

    submitLogin = (e) => {
        e.preventDefault();

        this.setState(prevState => ({ submit: !prevState.submit }));
        let username = this.username.current.value,
            password = this.password.current.value;

        if (username.trim().length === 0 || password.trim().length === 0) {
            this.setState(prevState => ({ submit: !prevState.submit }));
            return false;
        }

        let requestBody = {
            query: `
                 query ($username: String!, $password: String!)
                    { Login(username: $username, password: $password)
                        {
                            token
                            userId
                        }
                    }   
            `,
            variables: {
                username: username,
                password: password
            }
        };

        fetch(apiConfig.uri, {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201){
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                let data = resData.data.Login;
                if (data.token) {
                    this.context.login(
                        data.token,
                        data.userId
                    )
                }
            })
            .catch(err => {
                console.log(err);
                this.onDismiss();
                this.setState(prevState => ({ submit: !prevState.submit }));
            });

    };

    onDismiss = () => {
        this.setState(prevState => ({ visible: !prevState.visible }));
    };

    render() {

        return (
            <Container>
                {this.state.submit && (
                    <Loader/>
                )}
                <Row>
                    <Col md={{ size:4, offset: 4 }} className="mt-5">
                        <form onSubmit={this.submitLogin}>
                            <h1 className="text-center">Login</h1>

                            <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss}>
                                Username atau password salah!
                            </Alert>
                            <FormGroup>
                                <label>Username</label>
                                <input className="form-control" type="text" ref={this.username}/>
                            </FormGroup>

                            <FormGroup>
                                <label>Password</label>
                                <input className="form-control" type="password" autoComplete="" ref={this.password} />
                            </FormGroup>

                            <FormGroup>
                                <button type="submit" className="btn btn-dark btn-block"> Login </button>
                            </FormGroup>
                        </form>
                        <div className="mt-5 p-3 shadow">
                            <h6 className="text-center">Demo Aplikasi Penggajian karyawan VinaToys</h6>
                            <hr/>
                            <Row>
                                <Col sm='5'>
                                    <p className="font-weight-bold">Login dengan:</p>
                                </Col>
                                <Col sm="7">
                                    <p>
                                        Username: admin<br/>
                                        Password: admin
                                    </p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Login;
