import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Col, Row, Container, FormGroup} from 'reactstrap';
import {graphql, compose} from 'react-apollo';
import {Karyawans, Gajians, Periodes, AddGajian, DelGajian} from '../../queries';
import * as FontAwesome from 'react-icons/lib/fa';
import { confirmAlert } from 'react-confirm-alert';
import DatePicker from 'react-datepicker';
import AuthContext from "../../context/auth-context";
import {formatRupiah, formatTanggal} from "../../helper/helper";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import "react-datepicker/dist/react-datepicker.css";
import Navigation from "../../component/navigation/main";
import {DetailSavedGajian} from "./modal";

class SetGajian extends Component {
    state = {
        startDate: this.props.history.location.state.start_date,
        endDate: this.props.history.location.state.end_date,
        setGaji: false,
        selectedKaryawan: null,
        paymentDate: new Date(),
        lembur: 0,
        gaji: 0,
        totalUangMakan: 0,
        totalTransport: 0,
        id_periode: this.props.history.location.state.id_periode,
        detailSavedGajian: null,
        detailModal: false
    };

    static contextType = AuthContext;

    constructor(props) {
        super(props);

        this.id_karyawan = React.createRef();
        this.jamLembur = React.createRef();
        this.jumlahHari = React.createRef();
    }

    handlePaymentDate = (date) => {
        this.setState({ paymentDate: date });
    };

    setLembur = () => {
        const lembur = this.jamLembur.current.value * 6000;
        this.setState({ lembur: lembur});
    };

    setGaji = () => {
        const jmlHari = this.jumlahHari.current.value;
        const karyawan = this.state.selectedKaryawan;

        const gaji = jmlHari * karyawan.gaji_pokok;
        const uang_makan = jmlHari * karyawan.uang_makan;
        const transport = jmlHari * karyawan.transport;

        this.setState({
            gaji: gaji,
            totalUangMakan: uang_makan,
            totalTransport: transport
        });
    };

    submitGajian = () => {

        this.props.AddGajian({
            variables: {
                id_periode: this.state.id_periode,
                id_karyawan: this.id_karyawan.current.value,
                jam_lembur: +this.jamLembur.current.value,
                jumlah_hari: +this.jumlahHari.current.value,
                total_gaji: +this.state.lembur + this.state.gaji + this.state.totalUangMakan + this.state.totalTransport,
                tanggal_pembayaran: this.state.paymentDate
            },
            refetchQueries: [{ query: Gajians }]
        });

        confirmAlert({
            title: 'Gajian karyawan',
            message: 'Gaji ' +this.state.selectedKaryawan.nama+ ' telah tersimpan',
            buttons: [
                {
                    label: 'Oke',
                    onClick: () => this.setState({
                        selectedKaryawan: null,
                        lembur: 0,
                        gaji: 0,
                        totalTransport: 0,
                        totalUangMakan: 0
                    })
                }
            ]
        });
    };

    tampilKaryawan = () => {
        const id = this.id_karyawan.current.value;
        const selected = this.props.Karyawans.find(data => data._id === id);
        this.setState({ selectedKaryawan: selected});
    };

    kembali = () => window.location.href ='/gajian'

    showCreatedGajian = (id) => {

        const gajian = this.props.Gajians.find(data => data._id === id);
        const karyawan = this.props.Karyawans.find(data => data._id === gajian.id_karyawan);
        const periode = this.props.Periodes.find(data => data._id === gajian.id_periode);

        this.setState(prevState => ({
            detailModal: !prevState.detailModal,
            detailSavedGajian: {
                gajian: gajian,
                karyawan: karyawan,
                periode: periode
            }
        }));
    };

    handleDelete = (id) => {
        confirmAlert({
            title: 'Yakin ingin menghapus?',
            message: 'Data akan terhapus secara permanen',
            buttons: [
                {
                    label: 'Ya',
                    onClick: () => {
                        this.props.DelGajian({
                            variables: {
                                id: id
                            },
                            refetchQueries: [{ query: Gajians }]
                        })
                    }
                },
                {
                    label: 'Tidak',
                    onClick: () => console.log('batal hapus')
                }
            ]
        });
    };

    toggle = () => {
        this.setState(prevState => ({ detailModal: !prevState.detailModal }));
    };

    render() {

        let getKaryawanIdsPerPeriode = [],
            unsavedGajian = [],
            getKaryawansPerPeriode = [],
            savedGajian = [],
            dataSavedGajian = [];

        if (!this.props.loading) {
                        getKaryawanIdsPerPeriode = this.props.Gajians
                .filter(data => data.id_periode === this.state.id_periode)
                .map(gpp => {
                    return gpp.id_karyawan;
                });

            unsavedGajian = this.props.Karyawans.filter(({ _id }) => !getKaryawanIdsPerPeriode.includes(_id));

            savedGajian = this.props.Karyawans.filter(({ _id }) => getKaryawanIdsPerPeriode.includes(_id));

            getKaryawansPerPeriode = this.props.Gajians
                .filter(data => data.id_periode === this.state.id_periode)
                .map(gpp => {
                    return gpp;
                });

            dataSavedGajian = getKaryawansPerPeriode.map(gajian => {
                    const filtered = savedGajian.find(data => data._id === gajian.id_karyawan);
                    return ({
                        _id: gajian._id,
                        nama: filtered.nama,
                        bagian: filtered.bagian,
                        periode: formatTanggal(this.state.startDate) +' s.d '+ formatTanggal(this.state.endDate),
                        jumlah_hari: gajian.jumlah_hari,
                        gaji_pokok: filtered.gaji_pokok,
                        uang_makan: filtered.uang_makan,
                        transport: filtered.transport,
                        jam_lembur: gajian.jam_lembur,
                        gaji_bersih: gajian.total_gaji,
                        tanggal_bayar: formatTanggal(gajian.tanggal_pembayaran)
                    })
                });
        }

        const formSetGaji = this.state.selectedKaryawan !== null && (
            <div className="shadow p-5 mt-4">

                <FormGroup row>
                    <Col xs="4">
                        <label>Nama</label>
                    </Col>
                    <Col xs="8">
                        <label>: {this.state.selectedKaryawan.nama}</label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Bagian</label>
                    </Col>
                    <Col xs="8">
                        <label>: {this.state.selectedKaryawan.bagian}</label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Periode</label>
                    </Col>
                    <Col xs="8">
                        <label>
                            : {formatTanggal(this.state.startDate)} s.d {formatTanggal(this.state.endDate)}
                        </label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Gaji Pokok</label>
                    </Col>
                    <Col xs="8">
                        <label>: Rp. {formatRupiah(this.state.selectedKaryawan.gaji_pokok)}</label>
                    </Col>
                </FormGroup>

                {this.state.selectedKaryawan.transport && (
                    <FormGroup row>
                        <Col xs="4">
                            <label>Transport</label>
                        </Col>
                        <Col xs="8">
                            <label>: Rp. {formatRupiah(this.state.selectedKaryawan.transport)}</label>
                        </Col>
                    </FormGroup>
                )}

                {this.state.selectedKaryawan.uang_makan && (
                    <FormGroup row>
                        <Col xs="4">
                            <label>Uang Makan</label>
                        </Col>
                        <Col xs="8">
                            <label>: Rp. {formatRupiah(this.state.selectedKaryawan.uang_makan)}</label>
                        </Col>
                    </FormGroup>
                )}

                <FormGroup row>
                    <Col xs="4">
                        <label>Jumlah Hari </label>
                    </Col>
                    <Col xs="4">
                        <input type="number" className="form-control" ref={this.jumlahHari} onChange={this.setGaji}/>
                    </Col>
                    <Col xs="4">
                        <label>Hari</label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Lembur </label>
                    </Col>
                    <Col xs="4">
                        <input type="number" className="form-control" ref={this.jamLembur} onChange={this.setLembur}/>
                    </Col>
                    <Col xs="4">
                        <label>Jam</label>
                    </Col>
                    <Col xs={{ size: 4, offset: 4 }}>
                        <label className="mt-2 ml-1">Rp. {formatRupiah(this.state.lembur)}</label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Jumlah Gaji</label>
                    </Col>
                    <Col xs="8">
                        <label>: Rp. {formatRupiah(this.state.gaji)}</label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Total Gaji</label>
                    </Col>
                    <Col xs="8">
                        <label>
                            : Rp. {formatRupiah(this.state.gaji + this.state.lembur
                            + this.state.totalUangMakan + this.state.totalTransport)}
                        </label>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col xs="4">
                        <label>Dibayar Tanggal</label>
                    </Col>
                    <Col xs="8">
                        <DatePicker className="form-control"
                                    dateFormat="dd/MM/yyyy"
                                    selected={this.state.paymentDate}
                                    onChange={this.handlePaymentDate}
                        />
                    </Col>
                </FormGroup>

                <FormGroup className="text-center">
                    <button className="btn btn-dark mt-4" onClick={this.submitGajian}>Simpan</button>
                </FormGroup>

            </div>
        );

        const listKaryawan = (
            <FormGroup className="mt-md-5 mt-3 shadow p-3">
                <label>Pilih Karyawan</label>
                <select className="form-control"
                        ref={this.id_karyawan}
                        defaultValue={this.state.selectedKaryawan}
                        onChange={this.tampilKaryawan}
                >
                    <option value="">Pilih</option>
                    { !this.props.loading && unsavedGajian.map((k) => {
                            return (
                                <option key={k._id} value={k._id}>{k.nama}</option>
                            )
                        })
                    }
                </select>
            </FormGroup>
        );

        const setGajiKaryawan = (
            <React.Fragment>
                <Col md="12">
                    <h2 className="text-center mt-5">
                        Gajian karyawan
                    </h2>
                    <h4 className="text-center">
                        periode {formatTanggal(this.state.startDate)} s.d {formatTanggal(this.state.endDate)}
                    </h4>
                </Col>

                <Col md="2" className="mt-2">
                    <button className="btn btn-lg btn-secondary mt-md-5 pb-2" onClick={this.kembali}>
                        <FontAwesome.FaArrowLeft />
                    </button>
                    { dataSavedGajian.length > 0 && (
                        <Link className="btn btn-lg btn-outline-dark mt-md-5 pb-2 ml-2 d-none d-md-inline-block"
                              to={{
                                  pathname: "/print",
                                  state: { dataGajian: dataSavedGajian }
                              }}
                        >
                            <FontAwesome.FaPrint />
                        </Link>
                    )}
                </Col>

                <Col md={{ size:6, offset:0 }} className="mt-md-2 mb-5">
                    {listKaryawan}
                    {formSetGaji}
                </Col>
            </React.Fragment>
        );

        const listSavedGajiKaryawan = (
            <Col md={{ size:4, offset: 0 }} className="mt-md-2">
                <div className="shadow p-3 mt-md-5 mb-5">
                    <h6 className="text-center">Gaji karyawan yang sudah tersimpan:</h6>

                    <div className="border p-3">
                        <table className="table table-borderless">
                            <tbody>
                            { !this.props.loading && dataSavedGajian.map((k) => {
                                return (
                                    <tr key={k._id}>
                                        <td>
                                            <button className="btn btn-link p-0 text-dark"
                                                    onClick={this.showCreatedGajian.bind(this, k._id)}
                                            >
                                                {k.nama}
                                            </button>
                                        </td>
                                        <td className="text-right">
                                            <button className="btn btn-danger btn-sm"
                                                    onClick={this.handleDelete.bind(this, k._id)}
                                            >
                                                <FontAwesome.FaTrash />
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                            }
                            </tbody>
                        </table>
                    </div>


                </div>
            </Col>
        );

        return (
            <React.Fragment>
                <Navigation/>
                <Container>
                    <Row>
                        {setGajiKaryawan}
                        {listSavedGajiKaryawan}
                    </Row>
                    {this.state.detailSavedGajian && (
                        <DetailSavedGajian
                            onCancel={this.toggle}
                            isOpen={this.state.detailModal}
                            data={this.state.detailSavedGajian}
                        />
                    )}

                </Container>
            </React.Fragment>
        );
    }
}

export default compose(
    graphql(Karyawans, {
        props: ({ data }) => ({
            loading: data.loading,
            Karyawans: data.Karyawans
        }),
    }),
    graphql(Periodes, {
        props: ({ data }) => ({
            loadingPeriodes: data.loading,
            Periodes: data.Periodes
        }),
    }),
    graphql(Gajians, {
        props: ({ data }) => ({
            loadingGajians: data.loading,
            Gajians: data.Gajians
        }),
    }),
    graphql(AddGajian, {name: 'AddGajian'}),
    graphql(DelGajian, {name: 'DelGajian'}),
)(SetGajian);
