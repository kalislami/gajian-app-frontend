import React from 'react';
import {Modal, ModalBody, ModalHeader, ModalFooter, FormGroup, Col} from 'reactstrap';
import {formatRupiah, formatTanggal} from "../../helper/helper";
import 'bootstrap/dist/css/bootstrap.min.css';

const DetailSavedGajian = (props) => (
    <Modal isOpen={props.isOpen} fade={false} toggle={props.onCancel}>
        <ModalHeader toggle={props.onCancel}>Informasi Gaji Karyawan</ModalHeader>
        <ModalBody>

            <FormGroup row>
                <Col xs="6">
                    <h5>{props.data.karyawan.nama} - {props.data.karyawan.bagian} </h5>
                </Col>
                <Col xs="6">
                    <label>
                        Dibayar Tanggal {formatTanggal(props.data.gajian.tanggal_pembayaran)}
                    </label>
                </Col>
            </FormGroup>

            <FormGroup row>
                <Col xs="4">
                    <label>Gaji Pokok</label>
                </Col>
                <Col xs="8">
                    <label>Rp. {formatRupiah(props.data.karyawan.gaji_pokok)}</label>
                </Col>
            </FormGroup>

            {props.data.karyawan.transport && (
                <FormGroup row>
                    <Col xs="4">
                        <label>Transport</label>
                    </Col>
                    <Col xs="4">
                        <label>Rp. {formatRupiah(props.data.karyawan.transport)}</label>
                    </Col>
                    <Col xs="4">
                        <label>(x{props.data.gajian.jumlah_hari} Hari)</label>
                    </Col>
                </FormGroup>
            )}

            {props.data.karyawan.uang_makan && (
                <FormGroup row>
                    <Col xs="4">
                        <label>Uang Makan</label>
                    </Col>
                    <Col xs="4">
                        <label>Rp. {formatRupiah(props.data.karyawan.uang_makan)}</label>
                    </Col>
                    <Col xs="4">
                        <label>(x{props.data.gajian.jumlah_hari} Hari)</label>
                    </Col>
                </FormGroup>
            )}

            <FormGroup row>
                <Col xs="4">
                    <label>Jumlah Gaji</label>
                </Col>
                <Col xs="4">
                    <label>Rp. {formatRupiah(props.data.gajian.jumlah_hari * props.data.karyawan.gaji_pokok)}</label>
                </Col>
                <Col xs="4">
                    <label>({props.data.gajian.jumlah_hari} Hari)</label>
                </Col>
            </FormGroup>

            <FormGroup row>
                <Col xs="4">
                    <label>Lembur</label>
                </Col>
                <Col xs="4">
                    <label>Rp. {formatRupiah(props.data.gajian.jam_lembur * 6000)}</label>
                </Col>
                <Col xs="4">
                    <label>({props.data.gajian.jam_lembur} Jam)</label>
                </Col>
            </FormGroup>

            <FormGroup row>
                <Col xs="4">
                    <label>Total Gaji</label>
                </Col>
                <Col xs="8">
                    <label>
                        Rp. {formatRupiah(props.data.gajian.total_gaji)}
                    </label>
                </Col>
            </FormGroup>

        </ModalBody>
        <ModalFooter>
            <button type="button" className="btn btn-secondary" onClick={props.onCancel}>Tutup</button>
        </ModalFooter>
    </Modal>
);

export {DetailSavedGajian}