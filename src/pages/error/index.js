import React, {Component} from 'react';
import { Col, Row, Container } from 'reactstrap';
import {Link} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
class ErrorPage extends Component {

    render() {

        return (
            <Container>
                <Row>
                    <Col md={{ size:8, offset: 2 }} className="mt-5">
                        <div className="mt-5 p-3 shadow">
                            <h6 className="text-center">Maaf terdapat error pada sistem</h6>
                            <hr/>
                            <Row>
                                <Col sm='12'>
                                    <p className="font-weight-bold">Kemungkinan disebabkan karena service API atau server mati</p>
                                </Col>
                                <Col sm="12">
                                    <p> Silahkan reload dengan klik <Link className="text-primary" to={{pathname: "/" }}>
                                        disini
                                        </Link>
                                    </p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default ErrorPage;
