import React from 'react';
import {FormGroup, Row, Col} from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

const editForm = (props) => (
    <React.Fragment>
        <FormGroup>
            <input type="hidden" defaultValue={props.data._id} ref={props.id} />
            <label>Nama</label>
            <input className="form-control" type="text" defaultValue={props.data.nama} ref={props.nama} />
        </FormGroup>

        <FormGroup>
            <label>Bagian</label>
            <select className="form-control" defaultValue={props.data.bagian} ref={props.bagian}>
                <option value="Admin">Admin</option>
                <option value="Cutting">Cutting</option>
                <option value="Driver">Driver</option>
                <option value="Finishing">Finishing</option>
                <option value="Helper">Helper</option>
                <option value="Packing">Packing</option>
                <option value="Teknisi">Teknisi</option>
                <option value="Sampel">Sampel</option>
                <option value="Sewing">Sewing </option>
            </select>
        </FormGroup>

        <FormGroup className="my-4">
            <label>Gaji Pokok</label>
            <Row>
                <Col xs="1">
                    <label className="mt-1">Rp.</label>
                </Col>
                <Col xs="11">
                    <input className="form-control" type="number" defaultValue={props.data.gaji_pokok} ref={props.gaji_pokok}/>
                </Col>
            </Row>
        </FormGroup>

        <FormGroup className="my-4">
            <label>Uang Makan</label>
            <Row>
                <Col xs="1">
                    <label className="mt-1">Rp.</label>
                </Col>
                <Col xs="11">
                    <input className="form-control" type="number" defaultValue={props.data.uang_makan} ref={props.uang_makan}/>
                </Col>
            </Row>
        </FormGroup>

        <FormGroup className="my-4">
            <label>Transport</label>
            <Row>
                <Col xs="1">
                    <label className="mt-1">Rp.</label>
                </Col>
                <Col xs="11">
                    <input className="form-control" type="number" defaultValue={props.data.transport} ref={props.transport}/>
                </Col>
            </Row>
        </FormGroup>

        <FormGroup>
            <label>Lain-lain</label>
            <Row>
                <Col xs="1">
                    <label className="mt-1">Rp.</label>
                </Col>
                <Col xs="11">
                    <input className="form-control" type="number" defaultValue={props.data.apresiasi} ref={props.apresiasi}/>
                </Col>
            </Row>
        </FormGroup>

        <FormGroup>
            <button type="button" className="btn btn-secondary mr-3" onClick={props.onCancel}>Batal</button>
            <button className="btn btn-primary" onClick={props.onConfirm}>Update</button>
        </FormGroup>
    </React.Fragment>
);

export default editForm;