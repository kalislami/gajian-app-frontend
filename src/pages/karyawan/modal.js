import React from 'react';
import {Modal, ModalBody, ModalHeader, ModalFooter, FormGroup, Row, Col} from 'reactstrap';
import {formatRupiah} from "../../helper/helper";

import 'bootstrap/dist/css/bootstrap.min.css';

const AddModal = (props) => (
    <Modal isOpen={props.isOpen} fade={false} toggle={props.onCancel}>
        <ModalHeader toggle={props.onCancel}>Tambah Karyawan Baru</ModalHeader>
        <ModalBody>

            <FormGroup>
                <label>Nama</label>
                <input className="form-control" type="text" ref={props.nama} />
            </FormGroup>

            <FormGroup>
                <label>Bagian</label>
                <select className="form-control" ref={props.bagian}
                        defaultValue="Admin">
                    <option value="Admin">Admin</option>
                    <option value="Cutting">Cutting</option>
                    <option value="Driver">Driver</option>
                    <option value="Finishing">Finishing</option>
                    <option value="Helper">Helper</option>
                    <option value="Packing">Packing</option>
                    <option value="Teknisi">Teknisi</option>
                    <option value="Sampel">Sampel</option>
                    <option value="Sewing ">Sewing </option>
                </select>
            </FormGroup>

            <FormGroup className="my-4">
                <label>Gaji Pokok</label>
                <Row>
                    <Col xs="1">
                        <label className="mt-1">Rp.</label>
                    </Col>
                    <Col xs="11">
                        <input className="form-control" type="number" ref={props.gaji_pokok} />
                    </Col>
                </Row>
            </FormGroup>

            <FormGroup className="my-4">
                <label>Uang Makan</label>
                <Row>
                    <Col xs="1">
                        <label className="mt-1">Rp.</label>
                    </Col>
                    <Col xs="11">
                        <input className="form-control" type="number" ref={props.uang_makan} />
                    </Col>
                </Row>
            </FormGroup>

            <FormGroup className="my-4">
                <label>Transport</label>
                <Row>
                    <Col xs="1">
                        <label className="mt-1">Rp.</label>
                    </Col>
                    <Col xs="11">
                        <input className="form-control" type="number" ref={props.transport} />
                    </Col>
                </Row>
            </FormGroup>

            <FormGroup>
                <label>Lain-lain</label>
                <Row>
                    <Col xs="1">
                        <label className="mt-1">Rp.</label>
                    </Col>
                    <Col xs="11">
                        <input className="form-control" type="number" ref={props.apresiasi} />
                    </Col>
                </Row>
            </FormGroup>
        </ModalBody>
        <ModalFooter>
            <button type="button" className="btn btn-secondary" onClick={props.onCancel}>Batal</button>
            <button className="btn btn-primary" onClick={props.onConfirm}>Simpan</button>
        </ModalFooter>
    </Modal>
);

const DetailModal = (props) => (
    <Modal isOpen={props.isOpen} fade={false} toggle={props.onCancel}>
        <ModalHeader toggle={props.onCancel}>Informasi Karyawan</ModalHeader>
        <ModalBody>

            <FormGroup>
                <h4>{props.data.nama} - {props.data.bagian}</h4>
            </FormGroup>

            <FormGroup className="my-4">
                <h5>Gaji Pokok: <strong>Rp. {formatRupiah(props.data.gaji_pokok)}</strong></h5>
            </FormGroup>

            {props.data.uang_makan && (
                <FormGroup className="my-4">
                    <h5>Uang Makan: <strong>Rp. {formatRupiah(props.data.uang_makan)}</strong></h5>
                </FormGroup>
            )}

            {props.data.transport && (
                <FormGroup className="my-4">
                    <h5>Transport: <strong>Rp. {formatRupiah(props.data.transport)}</strong></h5>
                </FormGroup>
            )}

            {props.data.apresiasi && (
                <FormGroup>
                    <h5>Lain-lain: <strong>Rp. {formatRupiah(props.data.apresiasi)}</strong></h5>
                </FormGroup>
            )}

        </ModalBody>
        <ModalFooter>
            <button type="button" className="btn btn-secondary" onClick={props.onCancel}>Keluar</button>
        </ModalFooter>
    </Modal>
);

export {AddModal, DetailModal}