import React, {Component} from 'react';
import {Col, Row, Container, FormGroup} from 'reactstrap';
import {graphql, compose} from 'react-apollo';
import {Karyawans, AddKaryawan, DelKaryawan, UpdateKaryawan} from '../../queries';
import * as FontAwesome from 'react-icons/lib/fa';
import { confirmAlert } from 'react-confirm-alert';
import AuthContext from "../../context/auth-context";
import {formatRupiah} from "../../helper/helper";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import ExportCSV from"../../helper/export-excel";

// component
import {AddModal, DetailModal} from './modal';
import EditForm from './edit';
import Navigation from "../../component/navigation/main";

class Karyawan extends Component {
    state = {
        filterBagian: '',
        isEdit: false,
        addModal: false,
        detailModal: false,
        selected: null
    };

    static defaultProps = {
        data: {
            Karyawans: []
        }
    };
    static contextType = AuthContext;

    constructor(props) {
        super(props);

        this.nama = React.createRef();
        this.bagian = React.createRef();
        this.gaji_pokok = React.createRef();
        this.uang_makan = React.createRef();
        this.transport = React.createRef();
        this.apresiasi = React.createRef();

        this.editId = React.createRef();
        this.editNama = React.createRef();
        this.editBagian = React.createRef();
        this.editGajiPokok = React.createRef();
        this.editUangMakan = React.createRef();
        this.editTransport = React.createRef();
        this.editApresiasi = React.createRef();
    }

    handleFilter = (event) => {
        this.setState({ filterBagian: event.target.value });
    };

    toggle = (param) => {
        if (param === 'addModal') {
            this.setState(prevState => ({ addModal: !prevState.addModal }));
        } else {
            this.setState(prevState => ({ detailModal: !prevState.detailModal }));
        }
    };

    showDetail = (param, id) => {
        const data = this.props.data.Karyawans.find(e => e._id === id);

        if (param === 'detail') {
            this.setState(prevState => ({
                detailModal: !prevState.detailModal,
                selected: data
            }));
        } else {
            this.setState(prevState => ({
                isEdit: !prevState.isEdit,
                selected: data
            }));
        }
    };

    cancelEdit = () => {
        this.setState(prevState => ({
            isEdit: !prevState.isEdit,
            selected: null
        }));
    };

    addKaryawan = (e) => {
        e.preventDefault();

        const uang_makan = this.uang_makan.current.value > 0 ? +this.uang_makan.current.value : null,
            transport = this.transport.current.value > 0 ? +this.transport.current.value : null,
            apresiasi = this.apresiasi.current.value > 0 ? +this.apresiasi.current.value : null;

        this.props.AddKaryawan({
            variables: {
                nama: this.nama.current.value,
                bagian: this.bagian.current.value,
                gaji_pokok: +this.gaji_pokok.current.value,
                uang_makan: uang_makan,
                transport: transport,
                apresiasi: apresiasi
            },
            refetchQueries: [{ query: Karyawans }]
        });

        this.setState(prevState => ({ addModal: !prevState.addModal }));
    };

    updateKaryawan = (e) => {
        e.preventDefault();

        const uang_makan = this.editUangMakan.current.value > 0 ? +this.editUangMakan.current.value : null,
              transport = this.editTransport.current.value > 0 ? +this.editTransport.current.value : null,
              apresiasi = this.editApresiasi.current.value > 0 ? +this.editApresiasi.current.value : null;

        this.props.UpdateKaryawan({
            variables: {
                id: this.editId.current.value,
                nama: this.editNama.current.value,
                bagian: this.editBagian.current.value,
                gaji_pokok: +this.editGajiPokok.current.value,
                uang_makan: uang_makan,
                transport: transport,
                apresiasi: apresiasi
            },
            refetchQueries: [{ query: Karyawans }]
        });

        this.setState(prevState => ({
            isEdit: !prevState.isEdit,
            selected: null
        }));
    };

    deleteKaryawan = (id, nama) => {

        confirmAlert({
            title: 'Hapus karyawan',
            message: 'hapus ' + nama + ' dari daftar karyawan?',
            buttons: [
                {
                    label: 'Ya',
                    onClick: () => this.props.DelKaryawan({
                                        variables: {
                                            id: id
                                        },
                                        refetchQueries: [{ query: Karyawans }]
                                    })
                },
                {
                    label: 'Tidak',
                    onClick: () => console.log('tidak jadi menghapus ' + nama)
                }
            ]
        });
    };

    render() {

        let totalKaryawan = 0;

        if (!this.props.data.loading) {
            totalKaryawan = this.state.filterBagian === '' ? this.props.data.Karyawans.length
                : this.props.data.Karyawans.filter(item => item.bagian === this.state.filterBagian).length;
        }

        const list = this.props.data.loading ? (
                <tr>
                    <td colSpan="2">Loading data...</td>
                </tr>
            ) : this.props.data.Karyawans ? this.props.data.Karyawans.map((k) => {

                const data = (
                    <tr key={k._id}>
                        <td>{k.nama}</td>
                        <td>{k.bagian}</td>
                        <td>Rp. {formatRupiah(k.gaji_pokok)}</td>
                        <td>
                            <div className="btn-group" role="group">
                                <button type="button" className="btn btn-info" onClick={this.showDetail.bind(this, 'detail', k._id)}>
                                    <FontAwesome.FaInfo />
                                </button>
                                <button type="button" className="btn btn-dark" onClick={this.showDetail.bind(this, 'edit', k._id)}>
                                    <FontAwesome.FaPencil />
                                </button>
                                <button type="button" className="btn btn-danger" onClick={this.deleteKaryawan.bind(this, k._id, k.nama)}>
                                    <FontAwesome.FaTrash />
                                </button>
                            </div>
                        </td>
                    </tr>
                );

                if (this.state.filterBagian === ''){
                    return data;
                } else {
                    if (k.bagian === this.state.filterBagian){
                        return data;
                    } else {
                        return null;
                    }
                }
            }) : this.props.history.push({
                pathname: "/error"
            });

        const table = (
            <table className="table table-hover">
                <thead>
                <tr>
                    <th colSpan="4" className="text-right">
                        Total {totalKaryawan} Karyawan
                    </th>
                </tr>
                <tr>
                    <th>Nama</th>
                    <th>Bagian</th>
                    <th>Gaji Pokok</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                {list}
                </tbody>
            </table>
        );

        const filterSection = (
            <Row className="mt-5">
                <Col md="6">
                    <FormGroup className="pt-md-4">
                        <button className="btn btn-success mr-1" onClick={this.toggle.bind(this, 'addModal')}>
                            <FontAwesome.FaPlus />
                        </button>
                        <ExportCSV csvData={this.props.data.Karyawans} fileName="karyawan" />
                    </FormGroup>
                </Col>
                <Col md="6">
                    <FormGroup>
                        <label>Filter Bagian</label>
                        <select className="form-control"
                                defaultValue={this.state.filterBagian} onChange={this.handleFilter}>
                            <option value="">Semua</option>
                            <option value="Admin">Admin</option>
                            <option value="Cutting">Cutting</option>
                            <option value="Driver">Driver</option>
                            <option value="Finishing">Finishing</option>
                            <option value="Helper">Helper</option>
                            <option value="Packing">Packing</option>
                            <option value="Teknisi">Teknisi</option>
                            <option value="Sampel">Sampel</option>
                            <option value="Sewing">Sewing </option>
                        </select>
                    </FormGroup>
                </Col>
            </Row>
        );

        const addModal = (
            <AddModal
                onCancel={this.toggle.bind(this, 'addModal')}
                onConfirm={this.addKaryawan}
                isOpen={this.state.addModal}
                nama={this.nama}
                bagian={this.bagian}
                gaji_pokok={this.gaji_pokok}
                uang_makan={this.uang_makan}
                transport={this.transport}
                apresiasi={this.apresiasi}
            />
        );

        const detailModal = (
            <DetailModal
                onCancel={this.toggle.bind(this, 'detail')}
                isOpen={this.state.detailModal}
                data={this.state.selected}
            />
        );

        const editForm = (
            <EditForm
                onCancel={this.cancelEdit}
                onConfirm={this.updateKaryawan}
                data={this.state.selected}
                id={this.editId}
                nama={this.editNama}
                bagian={this.editBagian}
                gaji_pokok={this.editGajiPokok}
                uang_makan={this.editUangMakan}
                transport={this.editTransport}
                apresiasi={this.editApresiasi}
            />
        );

        return (
            <React.Fragment>
                <Navigation/>
                <Container>
                    <Row>
                        <Col md={{ size:8, offset: 2 }} className="mt-md-5">
                            {this.state.isEdit ? (
                                <React.Fragment>
                                    <h2 className="text-center">Edit Karyawan</h2>
                                    {editForm}
                                </React.Fragment>
                            ) : (
                                <React.Fragment>
                                    <h2 className="text-center">Daftar Karyawan</h2>
                                    {filterSection}
                                    {table}
                                </React.Fragment>
                            )}
                        </Col>
                    </Row>
                    {addModal}
                    {this.state.detailModal && detailModal}
                </Container>
            </React.Fragment>
        );
    }
}

export default compose(
    graphql(Karyawans),
    graphql(AddKaryawan, {name: 'AddKaryawan'}),
    graphql(DelKaryawan, {name: 'DelKaryawan'}),
    graphql(UpdateKaryawan, {name: 'UpdateKaryawan'})
)(Karyawan);
