import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Col, Row, Container, FormGroup} from 'reactstrap';
import {graphql, compose} from 'react-apollo';
import {Periodes, AddPeriode, DelPeriode, Gajians, DelGajian, PeriodesByDate} from '../../queries';
import * as FontAwesome from 'react-icons/lib/fa';
import { confirmAlert } from 'react-confirm-alert';
import DatePicker from 'react-datepicker';
import AuthContext from "../../context/auth-context";
import {formatTanggal} from "../../helper/helper";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import "react-datepicker/dist/react-datepicker.css";
import Navigation from "../../component/navigation/main";
import Loader from "../../component/loader/loader";

class Gajian extends Component {

    state = {
        startDate: new Date(),
        endDate: new Date(),
        loadingSubmit: false
    };

    static contextType = AuthContext;

    handleChangeStart = (date) => {
        this.setState({ startDate: date });
    };

    handleChangeEnd = (date) => {
        this.setState({ endDate: date });
    };

    submitDate = async () => {
        this.setState(prevState => ({
            loadingSubmit: !prevState.loadingSubmit
        }));

        const add = await this.props.AddPeriode({
            variables: {
                startDate: this.state.startDate.toISOString(),
                endDate: this.state.endDate.toISOString()
            },
            refetchQueries: [{ query: Periodes }]
        });

        const {_id, startDate, endDate} = add.data.createPeriode
        
        this.props.history.push({
            pathname: "/set-gajian",
            state: {
                id_periode: _id,
                start_date: startDate,
                end_date: endDate
            }
        })
    };

    handleDelete = (id) => {

        confirmAlert({
            title: 'Yakin ingin menghapus?',
            message: 'Data akan terhapus secara permanen',
            buttons: [
                {
                    label: 'Ya',
                    onClick: () => {
                        this.props.DelPeriode({
                            variables: {
                                id: id
                            },
                            refetchQueries: [{ query: PeriodesByDate }, {query: Gajians}]
                        });
                    }
                },
                {
                    label: 'Tidak',
                    onClick: () => console.log('batal hapus')
                }
            ]
        });
    };

    render() {

        const setPeriode = (
            <Col md={{ size:6, offset: 1 }} className="mt-5">

                <h2 className="text-center">Buat laporan gajian per periode</h2>
                <div className="shadow p-4 mt-5">
                    <Row>
                        <Col md="4">
                            <FormGroup>
                                <label>Dari tanggal:</label>
                                <br/>
                                <DatePicker className="form-control"
                                            dateFormat="dd/MM/yyyy"
                                            selected={this.state.startDate}
                                            selectsStart
                                            startDate={this.state.startDate}
                                            endDate={this.state.endDate}
                                            onChange={this.handleChangeStart}
                                />
                            </FormGroup>
                        </Col>

                        <Col md="4">
                            <FormGroup>
                                <label>Sampai tanggal:</label>
                                <br/>
                                <DatePicker className="form-control"
                                            dateFormat="dd/MM/yyyy"
                                            selected={this.state.endDate}
                                            selectsEnd
                                            startDate={this.state.startDate}
                                            endDate={this.state.endDate}
                                            onChange={this.handleChangeEnd}
                                            minDate={this.state.startDate}
                                />
                            </FormGroup>
                        </Col>

                        <Col md="4">
                            <FormGroup>
                                <button className="btn btn-dark" style={{ marginTop: '1.75rem' }} onClick={this.submitDate}>Submit</button>
                            </FormGroup>
                        </Col>
                    </Row>

                </div>
            </Col>
        );

        const listPeriode = (
            <Col md={{ size:4, offset: 0 }} className="mt-md-5">
                <div className="shadow p-3 my-5">
                    <h6 className="text-center">Daftar Periode Gajian</h6>

                    <div className="border p-2">
                        <table className="table table-borderless">
                            <tbody>
                            {   this.props.loadingPeriodes ? (
                                <tr>
                                    <td colSpan="2">Loading data... </td>
                                </tr>
                            ) : this.props.Periodes ? this.props.Periodes.map(p => {
                                return (
                                    <tr key={p._id}>
                                        <td>
                                            <Link className="text-dark" to={{
                                                pathname: "/set-gajian",
                                                state: {
                                                    id_periode: p._id,
                                                    start_date: p.startDate,
                                                    end_date: p.endDate
                                                }
                                            }}>
                                                {formatTanggal(p.startDate)} s.d {formatTanggal(p.endDate)}
                                            </Link>
                                        </td>
                                        <td className="text-right">
                                            <button className="btn btn-danger btn-sm" onClick={this.handleDelete.bind(this, p._id)}>
                                                <FontAwesome.FaTrash />
                                            </button>
                                        </td>
                                    </tr>
                                )
                            }) : 
                                this.props.history.push({
                                    pathname: "/error"
                                })   
                            }
                            </tbody>
                        </table>
                    </div>


                </div>
            </Col>
        );

        return (
            <React.Fragment>
                <Navigation/>
                {this.state.loadingSubmit && (
                    <Loader/>
                )}
                <Container>
                    <Row>
                        {setPeriode}
                        {listPeriode}
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default compose(
    graphql(Gajians, {
        props: ({ data }) => ({
            loadingGajians: data.loading,
            Gajians: data.Gajians
        }),
    }),
    graphql(PeriodesByDate, {
        props: ({ data }) => ({
            loadingPeriodes: data.loading,
            Periodes: data.PeriodesByDate
        }),
    }),
    graphql(AddPeriode, {name: 'AddPeriode'}),
    graphql(DelPeriode, {name: 'DelPeriode'}),
    graphql(DelGajian, {name: 'DelGajian'}),
)(Gajian);