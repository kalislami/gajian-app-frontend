import React, {Component, useRef} from 'react';
import ReactToPrint from 'react-to-print';
import {Col, Row, Container} from 'reactstrap';
import {formatRupiah} from "../../helper/helper";

import 'bootstrap/dist/css/bootstrap.min.css';
import './style/cetak.css';

class ComponentToPrint extends Component {

    render() {
        return (
            <Container>
                <Row className="p-5" >
                    {this.props.data.map(gajian => {
                        return (
                            <Col xs="6" key={gajian._id} className="mb-4">
                                <div className="border p-2">
                                    <h2 className="text-center">Slip Gaji</h2>
                                    <table className="table table-borderless border-top">
                                        <tbody>
                                        <tr>
                                            <th colSpan="4" className="text-center">Dibayar tanggal {gajian.tanggal_bayar}</th>
                                        </tr>
                                        <tr>
                                            <th>Nama</th>
                                            <td>:</td>
                                            <th colSpan="2">{gajian.nama}</th>
                                        </tr>
                                        <tr>
                                            <td>Bagian</td>
                                            <td>:</td>
                                            <td colSpan="2">{gajian.bagian}</td>
                                        </tr>
                                        <tr>
                                            <td>Periode</td>
                                            <td>:</td>
                                            <td colSpan="2">{gajian.periode}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah hari</td>
                                            <td>:</td>
                                            <td colSpan="2">{gajian.jumlah_hari} hari</td>
                                        </tr>
                                        <tr>
                                            <td>Gaji pokok</td>
                                            <td>:</td>
                                            <td colSpan="2">Rp. {formatRupiah(gajian.gaji_pokok)}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah gaji</td>
                                            <td>:</td>
                                            <td colSpan="2">Rp. {formatRupiah(gajian.gaji_pokok * gajian.jumlah_hari)}</td>
                                        </tr>

                                        {gajian.uang_makan && (
                                            <tr>
                                                <td>Uang makan</td>
                                                <td>:</td>
                                                <td colSpan="2">Rp. {formatRupiah(gajian.uang_makan)}</td>
                                            </tr>
                                        )}

                                        {gajian.uang_makan && (
                                            <tr>
                                                <td>Transport</td>
                                                <td>:</td>
                                                <td colSpan="2">Rp. {formatRupiah(gajian.uang_makan)}</td>
                                            </tr>
                                        )}

                                        <tr>
                                            <td>Lembur</td>
                                            <td>:</td>
                                            <td>Rp. {formatRupiah(gajian.jam_lembur * 6000)}</td>
                                            <td>({gajian.jam_lembur} Jam)</td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4"> </td>
                                        </tr>
                                        <tr>
                                            <th>Gaji bersih</th>
                                            <td>:</td>
                                            <th colSpan="2">Rp. {formatRupiah(gajian.gaji_bersih)}</th>
                                        </tr>
                                        <tr className="border-top">
                                            <th colSpan="4" className="text-center pt-3">Terima kasih atas kerjasamanya</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </Col>
                        )
                    })}
                </Row>
            </Container>
        );
    }
}

const Cetak = (props) => {
    const componentRef = useRef();
    return (
        <div>
            <ReactToPrint
                trigger={() => <button className="btn btn-success rounded-0">Print this out!</button>}
                content={() => componentRef.current}
            />
            <ComponentToPrint ref={componentRef} data={props.location.state.dataGajian} />
        </div>
    );
};

export default Cetak;