const {formatRupiah} = require('./helper/helper');

test('format rupiah', () => {
    expect(formatRupiah(100)).toBe('100');
    expect(formatRupiah(1000)).toBe('1.000');
    expect(formatRupiah(10000)).toBe('10.000');
    expect(formatRupiah(1000000)).toBe('1.000.000');
});