import React, {Component} from 'react';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import AuthContext from './context/auth-context';

// pages
import LoginPage from './pages/login/index';
import KaryawanPage from './pages/karyawan/index';
import GajianPage from './pages/gajian/index';
import PrintPage from './pages/print/cetak';
import SetGajianPage from './pages/set-gajian/index';
import { apiConfig } from './config/config';
import ErrorPage from './pages/error';

// apollo client setup
const client = new ApolloClient({
    uri: apiConfig.uri
});

class App extends Component {
    state = {
        token: null,
        userId: null,
        isOpenNavbar: false
    };


    login = (token, userId) => {
        this.setState({
            token: token,
            userId: userId
        });

        localStorage.setItem('token', token);
        localStorage.setItem('userId', userId);
    };

    logout = () => {
        this.setState({
            token: null,
            userId: null
        });

        localStorage.removeItem('token');
        localStorage.removeItem('userId');
    };

    componentDidMount() {
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');

        this.setState({
            token: token,
            userId: userId
        });
    }

    render() {
        return (
            <BrowserRouter>
                <React.Fragment>
                    <AuthContext.Provider
                        value={{
                            token: this.state.token,
                            userId: this.state.userId,
                            login: this.login,
                            logout: this.logout
                        }}>

                        <ApolloProvider client={client}>

                            <Switch>
                                <Redirect from="/" to="/login" exact /> 
                                <Route path="/error" component={ErrorPage} />

                                {this.state.token ? ( 
                                    <>
                                        <Redirect from="/login" to="/gajian" exact /> 
                                        <Route path="/karyawan" component={KaryawanPage} />
                                        <Route path="/gajian" component={GajianPage} />
                                        <Route path="/print" component={PrintPage} />
                                        <Route path="/set-gajian" component={SetGajianPage} />
                                    </>
                                ) : (
                                    <>
                                    <Route path="/login" component={LoginPage} />
                                    <Redirect to="/login" exact />
                                    </>
                                )}
                            </Switch>
                        </ApolloProvider>

                    </AuthContext.Provider>
                </React.Fragment>
            </BrowserRouter>
        );
    }
}

export default App;
