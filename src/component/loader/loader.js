import React from 'react';
import './loader.css';

const loader = () => (
    <React.Fragment>
        <div className="text-center">
            <div className="lds-hourglass"></div>
            <span className="lds-label">Loading</span>
        </div>
        <div className="loader-backdrop"> </div>
    </React.Fragment>
);

export default loader;