import React from 'react';
import { NavLink } from 'react-router-dom';
import AuthContext from '../../context/auth-context';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    Container} from 'reactstrap';
import * as FontAwesome from "react-icons/lib/fa";
import 'bootstrap/dist/css/bootstrap.min.css';

class navigation extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <AuthContext.Consumer>
                {(context) => (
                    <div>
                        <Navbar color="dark" light expand="md" dark>
                            <Container>
                                <NavbarBrand><em className="text-light">VinaToys</em></NavbarBrand>
                                <NavbarToggler onClick={this.toggle} />
                                <Collapse isOpen={this.state.isOpen} navbar>
                                    <Nav className="ml-auto" navbar>
                                        {context.token && (
                                            <React.Fragment>
                                                <NavItem className="text-center text-md-left">
                                                    <NavLink className="nav-link" to="/gajian">
                                                        <h6>Gajian</h6>
                                                    </NavLink>
                                                </NavItem>
                                                <NavItem className="text-center text-md-left">
                                                    <NavLink className="nav-link" to="/karyawan">
                                                        <h6>Karyawan</h6>
                                                    </NavLink>
                                                </NavItem>
                                                <NavItem className="text-center text-md-left">
                                                    <button onClick={context.logout}
                                                            className="btn btn-lg btn-dark ml-md-5 p-0">
                                                        <FontAwesome.FaSignOut />
                                                    </button>
                                                </NavItem>
                                            </React.Fragment>
                                        )}
                                    </Nav>
                                </Collapse>
                            </Container>
                        </Navbar>
                    </div>
                )}
            </AuthContext.Consumer>
        )

    }
}

export default navigation;